//
//  main.m
//  Bimbooo
//
//  Created by Joel Clanzett on 17.01.16.
//  Copyright © 2016 Joel Clanzett. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
